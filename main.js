import Vue from 'vue'
import App from './App'
import Vuex from 'vuex'
import store from './store'  

Vue.prototype.$store = store  
Vue.config.productionTip = false

App.mpType = 'app'

// 引入全局uView
import uView from 'uview-ui'
Vue.use(uView);
Vue.use(Vuex)
const app = new Vue({
    ...App,
	//挂载
	store
})
app.$mount()
