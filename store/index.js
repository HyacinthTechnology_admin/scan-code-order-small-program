import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
		token: '',
		ShopNumber: 0,//订单数量
		number:0,//桌号
	},
    mutations: {
		addShopNumber (state) {
		  state.ShopNumber++
		},
		unpNumber (state,num){
			state.number = num;
		},

	},
    actions: {
		addShopNumber (context) {
		  context.commit('addShopNumber')
		},
		updNumber (context,num) {
		  context.commit('unpNumber',num)
		}
	}
})
export default store